# Backup mysql database

Ce script bash permet de sauvegarder une base de données MySQL.

Il suffit de changer les variables du fichier $HOME/.backupconf pour les logs du serveur MySQL.

---

Pour sauvegarder une base de données, il faut changer les variables dans le fichier $HOME/.backupconf.

Puis lancer le script : 
```
./backup-mysql.sh -o
```

---

Pour sauvegarder toutes les base de données, il faut lancer le script avec l'option -a :
```
./backup-mysql.sh -a
```

---

Pour restaurer une base de données, il faut passer les commandes :
```
mysql -u[user] -p[password]

mysql> CREATE DATABASE [dbname];
mysql> quit

mysql -u[user] -p[password] [dbname] < dbbackup.sql
```
