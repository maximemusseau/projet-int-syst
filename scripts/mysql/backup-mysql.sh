#!/bin/bash

set -e

## Source file $HOME/.backupconf where $host $port $user $pass $dirsave $dbname are stored
 
source $HOME/.backupconf

logdate=$(date +"%F_%Hh%Mm%Ss")
logerror='[Warning] Using a password on the command line can be insecure'

save_one_db() {

	## Check if directory path exists
	if [ -d $dirsave/$dbname ]; then
		printf '\nBackup will start\n'
	else
		## Create directory
		mkdir -p $dirsave
		mkdir -p $dirsave/$dblname
		printf "\nCreated $dirsave/$dbname\n"
	fi

	countsave="$(find $HOME/$dirsave/$dbname -type f | wc -l)"
	oldestsave="$(find $HOME/$dirsave/$dbname -type f | sort | head -1)"

	## Remove oldest save
	if [ $countsave == '5' ]; then
		printf "\nRemoving oldest save $oldestsave\n"
		rm $oldestsave 2>> ~/dberror.log
		printf '\nBackup is starting\n'
	else
		printf '\nBackup is starting\n'
	fi	

	## Saving database
	mysqldump -h$host -P$port -u$user -p$pass $dbname 2>> ~/dberror.log | grep -v "$logerror" > $dirsave/$dbname/$logdate.sql
	printf "\nBackup has been saved in $dirsave/$dbname/$logdate.sql\n" | tee -a dbsucces.log
}


save_all_db() {
	## Store all database in list
	dblist="$(mysql -h$host -P$port -u$user -p$pass -e "show databases" --batch --skip-column-names 2>> ~/dberror.log | grep -v "$logerror")"
	
	## Save each database in their respective directory
	for db in $dblist; do
		if [ -d $dirsave/$db ]; then
			:
		else	
			mkdir -p $dirsave
			mkdir -p $dirsave/$db
			printf "\nCreated $dirsave/$db\n"
		fi

		countsaveall="$(find $HOME/$dirsave/$db -type f | wc -l)"
		oldestsaveall="$(find $HOME/$dirsave/$db -type f | sort | head -1)"
		if [ $countsaveall == '5' ]; then
			printf "\nRemoving oldest save $oldestsaveall\n"
			rm $oldestsaveall 2>> ~/dberror.log
		else
			:
		fi

		mysqldump -h$host -P$port -u$user -p$pass $db 2>> ~/dberror.log | grep -v "$logerror" > $dirsave/$db/$logdate.sql
		printf "\nBackup have been saved in $dirsave/$db/$logdate.sql\n" | tee -a dbsucces.log
	done
}

## Set options
while [ -n "$1" ]; do 
	case "$1" in
	-a) save_all_db ;;
	-o) save_one_db ;;
	*) echo "Option $1 not recognized" ;;
	esac
	shift
done
