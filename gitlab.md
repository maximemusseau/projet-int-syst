# Installation manuelle Gitlab

Voici le déroulement d'une installation basique sur **CentOS7** :

---

## Configuration du serveur
Pour **100** utilisateurs Gitlab, il est recommandé :
* **2** coeurs
* **4GB** de RAM + **4GB** de SWAP
* Une base de données **PostgreSQL**
* **10GB** d'espace disque

---

## Prérequis et installation des dépendances

* On installe le package **policycoreutils-python** qui contient des outils de gestion pour un environnement Linux.
* On installe ensuite **postfix** qui sera notre serveur mail pour plus tard.
* On ouvre les ports **80** et **443** avec les commandes : 
```
firewall-cmd --permanent --add-service=http
firewall-cmd --permanent --add-service=https
```
On relance ensuite le firewall pour prendre en compte ces changements.

---

## Installation du package Gitlab Omnibus

Cette installation se fera avec le package [Omnibus](https://docs.gitlab.com/omnibus/).

On télécharge tout d'abord un [script](https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh) qui installera les dépendances et initialisera le répertoire pour l'installation.

On installe ensuite Gitlab avec la commande :
```
yum install gitlab-ce
```

Une fois l'installation terminée, on édite le fichier **/etc/gitlab/gitlab.rb** qui contient la configuration du serveur.

On modifiera l'adresse IP ou le nom de domaine à la ligne :
```
external_url "mondomaine.com"
```

On peut maintenant reconfigurer Gitlab pour qu'il prenne en compte les changements :
```
gitlab-ctl reconfigure
```

On peut maintenant accéder à Gitlab sur notre navigateur web :
![](images/web.png)

