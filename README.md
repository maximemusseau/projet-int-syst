# Intégration Gitlab CE

Intégration Gitlab CE dans un réseau entreprise.

---

# Sommaire

* [Installation](#installation)
* [Exploitation](#exploitationgestionsauvegarde)

---

## Installation

### Prérequis pour déployer Gitlab

* [VirtualBox](https://www.virtualbox.org/)
* [Vagrant](https://www.vagrantup.com/)
* [Ansible](https://www.ansible.com/)

### Déploiement sur CentOS7

Créez un dossier et déposez-y les fichiers **Vagrantfile** et **gitlab-ce.yml**.

Configurez les IPs et les caractéristiques dans le **Vagrantfile** puis lancez le déploiement :
```
vagrant up
```

Après quelques minutes de configuration, on peut accéder au gitlab depuis son IP :
![](images/gitlab.png)


### Installation manuelle

Voici la documentation de [l'installation manuelle](gitlab.md) si vous ne voulez pas utiliser VirtualBox, Vagrant ou Ansible.

---

## Installation DNS

Installation de deux serveurs DNS

### Configuration des serveurs

* **CentOS7**
* **1 coeur**
* **1GB** de RAM
* **10GB** d'espace disque

### Installation des packages

Pour configurer nos serveurs, on a besoin des packages **bind** et **bind-utils**.
BIND signifie Berkeley Internet Name Domain. 
Cela va nous servir à convertir un nom de domaine en IP ou l'inverse.


### Configuration des fichiers

La fichier principal est **/etc/named.conf**. 
On y définie l'adresse où BIND va écouter, ainsi que les zones :

* **forward**
```
zone "intsystexample.com" IN {
	type master;
	file "fwd.intsystexample.com.db";
	allow-update { none; };
};
```

* **reverse**

```
zone "56.168.192.in-addr.arpa" IN {
	type master;
	file "56.168.192.db";
	allow-update { none; };
}; 
```

On crée ensuite les zones dans **/var/named/[nomdufichier]** :

* **/var/named/fwd.intsystexample.com.db**

On y configure les noms et les IPs qui qui seront convertis.

```
...

;Gitlab Name server
@      IN  NS      gitlab.intsystexample.com.

...

;IP address of Gitlab Name Server
gitlab    IN A      192.168.56.10

...
```

* **/var/named/56.128.192.db**

```
@ IN  SOA     primary.intsystexample.com. root.intsystexample.com. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@ IN  NS      primary.intsystexample.com.

;Reverse lookup for Name Server
8        IN  PTR     primary.intsystexample.com.

;PTR Record IP address to HostName
100      IN  PTR     www.intsystexample.com.
150      IN  PTR     mail.intsystexample.com.
```

### Règles firewall et /etc/resolv.conf

On ouvre le port 53 qui permet les requêtes DNS :
```
firewall-cmd --permanent --add-port=53/udp
firewall-cmd --reload
```

On rentre ensuite le serveur DNS dans le fichier **/etc/resolv.conf** qui regroupe les serveurs DNS que la machine utilise :
```
nameserver 192.168.56.30
```

On peut ensuite accéder depuis notre navigateur à Gitlab depuis un nom de domaine

![](images/dns.png)



---

## Exploitation/Gestion/Sauvegarde

Cette partie va décrire le fonctionnement technique (démarrage/arrêt du serveur), la gestion des utilisateurs ainsi que les procédures de sauvegarde/restauration.

---

### Exploitation

Après l'installation basique, le serveur se lance au démarrage. Voici les commandes principales pour le fonctionnement du serveur :
```
## Démarrage
gitlab-ctl start

## Arrêt
gitlab-ctl stop

## Redémarrage
gitlab-ctl restart

## Pour un seul service
gitlab-ctl [start/stop/restart] [service]
```

Tous les logs sont séparés par service dans /var/log/gitlab :

```
[root@localhost ~]# ls /var/log/gitlab
alertmanager    gitlab-rails      grafana    node-exporter      prometheus   redis-exporter
gitaly          gitlab-shell      logrotate  postgres-exporter  reconfigure  sidekiq
gitlab-monitor  gitlab-workhorse  nginx      postgresql         redis        unicorn
```

On retrouve une partie de ces logs sur l'interface web, dans la partie monitoring de l'administration :
![](images/logs.png)

---

### Gestion

La gestion des projets/utilisateurs/groupes se fait sur l'interface web. On accède, depuis un compte administrateur, à la zone administrateur :
![](images/gestion.png)

On a une vue globale sur les informations des utilisateurs et du serveur.

---

### Sauvegarde et restauration

La sauvegarde du serveur Gitlab se fait en deux étapes : 
* La première est la sauvegarde des fichiers de la configuration globale (/etc/gitlab/) :
```
sh -c 'umask 0077; tar -cf $(date "+etc-gitlab-%s.tar)" -C / etc/gitlab'
```

Cette commande permet d'archiver /etc/gitlab/ dans le répertoire courant.
On peut très bien mettre rajouter cette commande dans la crontab, pour planifier les sauvegardes.

Pour la restauration, il suffit de passer la commande :
```
tar -xf etc-gitlab-[date].tar -C /
```

L'archive sera décompressée dans /.
On peut renommer au préalable le répertoire existant pour ne pas le perdre.

On reconfigure ensuite gitlab pour finaliser la restauration :
```
gitlab-ctl reconfigure
```

* La deuxième et dernière étape est la sauvegarde du reste de la configuration gitlab :
```
[root@localhost ~]# gitlab-backup create
2019-09-07 16:10:26 +0200 -- Dumping database ...
Dumping PostgreSQL database gitlabhq_production ... [DONE]
2019-09-07 16:10:27 +0200 -- done
2019-09-07 16:10:27 +0200 -- Dumping repositories ...
2019-09-07 16:10:27 +0200 -- done
2019-09-07 16:10:27 +0200 -- Dumping uploads ...
2019-09-07 16:10:27 +0200 -- done
2019-09-07 16:10:27 +0200 -- Dumping builds ...
2019-09-07 16:10:27 +0200 -- done
2019-09-07 16:10:27 +0200 -- Dumping artifacts ...
2019-09-07 16:10:27 +0200 -- done
2019-09-07 16:10:27 +0200 -- Dumping pages ...
2019-09-07 16:10:27 +0200 -- done
2019-09-07 16:10:27 +0200 -- Dumping lfs objects ...
2019-09-07 16:10:27 +0200 -- done
2019-09-07 16:10:27 +0200 -- Dumping container registry images ...
2019-09-07 16:10:27 +0200 -- [DISABLED]
Creating backup archive: 1567865427_2019_09_07_12.2.4_gitlab_backup.tar ... done
Uploading backup archive to remote storage  ... skipped
Deleting tmp directories ... done
done
done
done
done
done
done
done
Deleting old backups ... skipping
Warning: Your gitlab.rb and gitlab-secrets.json files contain sensitive data
and are not included in this backup. You will need these files to restore a backup.
Please back them up manually.
Backup task is done.
```

Cette commande déjà intégrée à l'installation fait une sauvegarde complète, sauf des fichiers du répertoire /etc/gitlab/. On peut aussi l'ajouter à la crontab.

La sauvegarde est stockée par défaut dans /var/opt/gitlab/backups/.
Ce chemin peut être modifié dans le fichier /etc/gitlab/gitlab.rb à la ligne gitlab_rails['backup_path'].

Voici la procédure de la restauration :
```
## On met l'utilisateur git en owner
chown git.git /var/opt/gitlab/backups/[archive].tar

## On stoppe les services connectés à la base de données
gitlab-ctl stop unicorn
gitlab-ctl stop sidekiq

## On restore
gitlab-backup restore BACKUP=[archive].tar

## On reconfigure et relance le serveur
gitlab-ctl reconfigure
gitlab-ctl restart
```

Le serveur sera de nouveau opérationnel quelques minutes après.


